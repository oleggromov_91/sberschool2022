package easyCode.utils;

import java.util.Arrays;


public class ArgsPrinter {


    public static void printArgs(String... args) {
        Arrays.stream(args).forEach(System.out::println);
    }
}
